export const add = (num1: number, num2: number): number => num1 + num2

export const addAsync = async (num1: number, num2: number): Promise<number> => {
  const result = await new Promise<number>((resolve) => resolve(num1 + num2))
  return result
}
