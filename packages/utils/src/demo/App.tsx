import React, { useEffect, useState } from 'react'
import { add, addAsync } from '../lib'

type Status = 'idle' | 'calculating' | 'done'

function App() {
  const [sum, setSum] = useState(() => 0)
  const [status, setStatus] = useState<Status>(() => 'idle')

  useEffect(() => {
    ;(async function() {
      setStatus('calculating')
      const result = await addAsync(1, 1)
      setSum(result)
      setStatus('done')
    })()
  }, [])

  return (
    <div className='App'>
      <div>add: 1 + 1 = {add(1, 1)}</div>
      <div>
        <div>addAsync</div>
        <div>status: {status}</div>
        <div>1 + 1 = {sum}</div>
      </div>
    </div>
  )
}

export default App
