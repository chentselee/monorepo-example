# @mylib/utils

## 0.3.0

### Minor Changes

- b2cbaab: add subtract. add concurrently

## 0.2.0

### Minor Changes

- 3d7979c: add addAsync

## 0.1.0

### Minor Changes

- 311dec2: uitls: separate demo and lib modules
