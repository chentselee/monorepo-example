import React, { HTMLAttributes } from 'react'
import clsx from 'clsx'

interface Props extends HTMLAttributes<HTMLButtonElement> {
  size?: 'small' | 'medium' | 'large'
}

export const Button: React.FC<Props> = ({ size = 'medium', ...props }) => {
  return (
    <button
      onClick={() => console.log('clicked!')}
      className={clsx('bg-blue-500 text-white font-bold rounded hover:bg-blue-200', { 'px-2.5 py-1 text-xs': size === 'small', 'px-5 py-2': size === 'medium', 'px-9 py-3 text-2xl': size === 'large' })}
      {...props}
    />
  )
}
