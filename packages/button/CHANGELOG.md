# @mylib/button

## 1.2.0

### Minor Changes

- 9c38e3a: add sizes

## 1.1.0

### Minor Changes

- bcbe220: allow custom children
