# @mylib/typography

## 1.2.0

### Minor Changes

- 6c9823f: add paragraph

## 1.1.1

### Patch Changes

- a8aaa81: fix Heading2 export

## 1.1.0

### Minor Changes

- d082316: add Heading2
