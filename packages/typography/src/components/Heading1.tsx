import React, { HTMLAttributes } from 'react'

interface Props extends HTMLAttributes<HTMLHeadingElement> {}

export const Heading1: React.FC<Props> = (props) => {
  return <h1 className='font-bold text-xl' {...props} />
}
