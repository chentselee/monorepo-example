import React, { HTMLAttributes } from 'react'

interface Props extends HTMLAttributes<HTMLHeadingElement> {}

export const Heading2: React.FC<Props> = (props) => {
  return <h2 className='font-bold text-lg' {...props} />
}
