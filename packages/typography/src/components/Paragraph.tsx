import React, { HTMLAttributes } from 'react'

interface Props extends HTMLAttributes<HTMLParagraphElement> { }

export const Paragraph: React.FC<Props> = (props) => {
  return <p {...props} />
}
