.PHONY: dev-lib
dev-lib: 
		npx lerna run dev --scope app1 --scope docs --scope @mylib/utils --scope @mylib/typography --scope @mylib/button

.PHONY: dev-app
dev-app:
		(trap 'kill 0' INT; cd ./packages/app1 && yarn dev & cd ./packages/docs && yarn dev)
