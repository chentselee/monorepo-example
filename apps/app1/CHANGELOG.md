# app1

## 0.6.0

### Minor Changes

- 87546b2: add paragraph

### Patch Changes

- Updated dependencies [6c9823f]
- Updated dependencies [9c38e3a]
  - @mylib/typography@1.2.0
  - @mylib/button@1.2.0

## 0.5.3

### Patch Changes

- Updated dependencies [b2cbaab]
  - @mylib/utils@0.3.0

## 0.5.2

### Patch Changes

- Updated dependencies [3d7979c]
  - @mylib/utils@0.2.0

## 0.5.1

### Patch Changes

- Updated dependencies [311dec2]
  - @mylib/utils@0.1.0

## 0.5.0

### Minor Changes

- f42842d: move NotFound to separate dir

## 0.4.0

### Minor Changes

- 6ec81ce: add react-router. add About

## 0.3.0

### Minor Changes

- 3067c3c: add vanilla-extract

## 0.2.0

### Minor Changes

- 2897156: update @mylib/button

## 0.1.1

### Patch Changes

- 086ac6f: update app

## 0.1.0

### Minor Changes

- 62120a8: add Heading2
