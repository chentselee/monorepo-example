import React from 'react'
import { Heading1, Heading2 } from '@mylib/typography'
import { add } from '@mylib/utils'
import { styles } from './About.css'

const About = () => {
  return (
    <div className={styles.container}>
      <Heading1>About</Heading1>
      <Heading2>using package '@mylib/utils': 1 + 2 = {add(1, 2)}</Heading2>
    </div>
  )
}

export default About
