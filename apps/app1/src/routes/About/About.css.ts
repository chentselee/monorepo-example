import { style } from '@vanilla-extract/css'
import { calc } from '@vanilla-extract/css-utils'
import { theme } from 'src/theme.css'

export const styles = {
  container: style({
    display: 'grid',
    placeItems: 'center',
    placeContent: 'center',
    gap: calc.multiply(theme.spacing, 2),
  }),
}
