import React from 'react'
import { Button } from '@mylib/button'
import { Heading1, Heading2, Paragraph } from '@mylib/typography'
import { add } from '@mylib/utils'
import { styles } from './Home.css'

const Home = () => {
  return (
    <div className={styles.container}>
      <Heading1>Home</Heading1>
      <Heading2>app1111111</Heading2>
      <Heading2>heading2</Heading2>
      <Heading2>using package '@mylib/utils': 1 + 2 = {add(1, 2)}</Heading2>
      <Paragraph>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maiores, iusto.
      </Paragraph>
      <Button size='small' onClick={() => console.log('custom click')}>
        small
      </Button>
      <Button onClick={() => console.log('custom click')}>medium</Button>
      <Button size='large'>large</Button>
    </div>
  )
}

export default Home
