import { style } from '@vanilla-extract/css'
import { calc } from '@vanilla-extract/css-utils'
import { theme } from 'src/theme.css'

export const styles = {
  container: style({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    gap: calc.multiply(theme.spacing, 2),
  }),
}
