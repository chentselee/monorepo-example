import React, { Suspense } from 'react'
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import Home from './routes/Home'
import About from './routes/About'
import NotFound from './routes/NotFound'
import { styles } from './App.css'

function App() {
  return (
    <>
      <BrowserRouter basename={import.meta.env.MODE === 'production' ? '/monorepo-example' : ''}>
        <nav className={styles.nav}>
          <Link to='/' className={styles.link}>
            Home
          </Link>
          <Link to='/about' className={styles.link}>
            About
          </Link>
        </nav>
        <Suspense fallback={<div>loading...</div>}>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/about' exact component={About} />
            <Route path='*' component={NotFound} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    </>
  )
}

export default App
