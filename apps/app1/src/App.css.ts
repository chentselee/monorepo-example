import { style } from '@vanilla-extract/css'
import { calc } from '@vanilla-extract/css-utils'
import { theme } from 'src/theme.css'

export const styles = {
  nav: style({
    paddingBlock: calc.multiply(theme.spacing, 3),
    paddingInline: calc.multiply(theme.spacing, 5),
    display: 'flex',
    justifyContent: 'center',
    gap: calc.multiply(theme.spacing, 5),
  }),
  link: style({
    fontWeight: 250,
  }),
}
