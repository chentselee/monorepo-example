import { defineConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'
import { vanillaExtractPlugin } from '@vanilla-extract/vite-plugin'

const isGitlab = process.env.IS_GITLAB

export default defineConfig({
  base: isGitlab ? '/monorepo-example/' : '',
  plugins: [reactRefresh(), vanillaExtractPlugin()],
})
